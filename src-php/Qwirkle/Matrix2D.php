<?php

namespace Qwirkle;

/**
 * Matrix2D can give information, which is used to check the game-rules, about 
 * a set of tiles (or cordinates)
 */
class Matrix2D {

    const IS_NO_LINE = 0;
    const IS_ROW = 1;
    const IS_COLUMN = 2;
    const IS_SINGLE = 3;

    private $map = [];
    private $x_values = [];
    private $y_values = [];
    private $has_unique_positions = true;
    private $x_min;
    private $x_max;
    private $y_min;
    private $y_max;

    public function __construct(array $elements) {
        if (!$elements) {
            throw new \ErrorException("No elements given to create Matrix.");
        }
        foreach ($elements as $element) {
            $this->addUncachedElement($element[0], $element[1]);
        }
        $this->updateCache();
    }

    private function addUncachedElement(int $x, int $y) {
        if (isset($this->map[$x][$y])) {
            $this->has_unique_positions = false;
        } else {
            $this->map[$x][$y] = 1;
            $this->x_values[] = $x;
            $this->y_values[] = $y;
        }
    }

    private function updateCache() {
        $this->x_values = array_unique($this->x_values);
        $this->y_values = array_unique($this->y_values);
        $this->x_min = min($this->x_values);
        $this->x_max = max($this->x_values);
        $this->y_min = min($this->y_values);
        $this->y_max = max($this->y_values);
    }

    public function addElement(int $x, int $y) {
        $this->addUncachedElement($x, $y);
        $this->updateCache();
    }

    public function getElement(int $x, int $y) {
        return $this->map[$x][$y] ?? null;
    }

    public function getMap() {
        return $this->map;
    }

    public function elementsAreUnique(): bool {
        return $this->has_unique_positions;
    }

    public function getValuesY() {
        return $this->y_values;
    }

    public function getValuesX() {
        return $this->x_values;
    }

    public function getXmin() {
        return $this->x_min;
    }

    public function getXmax() {
        return $this->x_max;
    }

    public function getYmin() {
        return $this->y_min;
    }

    public function getYmax() {
        return $this->y_max;
    }

    /**
     * Single element: IS_ROW!
     * @param array $map
     * @return int
     */
    public function getLineDirection(): int {
        if (!$this->map) {
            return self::IS_NO_LINE;
        }
        if (count($this->y_values) === 1) {
            return count($this->x_values) === 1 ? self::IS_SINGLE : self::IS_ROW;
        } else if (count($this->x_values) === 1) {
            return self::IS_COLUMN;
        }
        return self::IS_NO_LINE;
    }

    public function getMaxCountItemsInLine(): int {
        $max_value = 0;
        for ($i = $this->getXmin(); $i <= $this->getXmax(); $i++) {
            $current_value = array_sum($this->map[$i] ?? []);
            $max_value = max([$current_value, $max_value]);
        }
        for ($i = $this->getYmin(); $i <= $this->getYmax(); $i++) {
            $current_value = array_sum(array_column($this->map, $i));
            $max_value = max([$current_value, $max_value]);
        }
        return $max_value;
    }

    public function __toString() {
        return print_r($this->map, true);
    }

}
