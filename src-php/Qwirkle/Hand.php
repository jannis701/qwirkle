<?php

namespace Qwirkle;

class Hand {

    protected $tiles = [];
    protected $last_swap_count = 0;

    public function __construct(array $tiles_init = []) {
        $this->tiles = $tiles_init;
    }

    public function isEmpty(): bool {
        return count($this->tiles) === 0;
    }

    public function getCount(): int {
        return count($this->tiles);
    }

    public function getHandValue(): int {
        $hand_map = new Matrix2D($this->tiles);
        return $hand_map->getMaxCountItemsInLine();
    }

    public function addTiles(array $tiles) {
        foreach ($tiles as $tile) {
            $this->tiles[] = $tile;
        }
    }

    public function removeTiles(array $tiles) {
        foreach ($tiles as $tile) {
            $this->removeTile($tile);
        }
    }

    public function removeTile(array $tile) {
        for ($i = 0; $i < count($this->tiles); $i++) {
            if ($this->tiles[$i] == $tile) {
                array_splice($this->tiles, $i, 1);
                return;
            }
        }
        throw new \ErrorException("Cannot remove tile: not in hand.");
    }

    public function containsTiles(array $tiles): bool {
        $test_hand = $this->copy();
        try {
            $test_hand->removeTiles($tiles);
        } catch (\ErrorException $ex) {
            return false;
        }
        return true;
    }

    public function swapTiles(array $tiles_going, array $tiles_coming): void {
        $this->removeTiles($tiles_going);
        $this->addTiles($tiles_coming);
        $this->last_swap_count = count($tiles_coming);
    }

    public function getLastSwapCount() {
        return $this->last_swap_count;
    }

    public function copy(): Hand {
        return new Hand($this->tiles);
    }

    public function getTiles(): array {
        return $this->tiles;
    }

    public function __toString() {
        return print_r($this->tiles, true);
    }

}
