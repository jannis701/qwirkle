<?php

namespace Qwirkle;

class Player {

    protected $name;
    protected $id;
    protected $wanted_qwirkle_size;
    protected $score_values = [];

    /**
     * 
     * @var Hand
     */
    protected $hand = null;
    protected $in_game = true;

    public function __construct(string $name, int $wanted_qwirkle_size = 6) {
        $this->name = $name;
        if ($wanted_qwirkle_size < 2 || $wanted_qwirkle_size > 10) {
            throw new \ErrorException("Invalid Qwirkle size: $wanted_qwirkle_size");
        }
        $this->wanted_qwirkle_size = $wanted_qwirkle_size;
        $this->id = preg_replace("/[^a-z0-9]/", "", strtolower($name));
        if (!$this->id) {
            throw new \ErrorException("Invalid Name.");
        }
    }

    public function setHand(Hand $hand): void {
        if ($this->hand) {
            throw new \ErrorException("Cannot set player's hand. Hand is already set.");
        }
        $this->hand = $hand;
    }

    public function getHand(): array {
        if (!$this->hand) {
            return [];
        }
        return $this->hand->getTiles();
    }

    public function setHandDebug(Hand $hand): void {
        $this->hand = $hand;
    }

    public function hasTiles(array $tiles): bool {
        return $this->hand->containsTiles($tiles);
    }

    public function swapTiles(array $tiles_going, array $tiles_coming): void {
        $this->hand->swapTiles($tiles_going, $tiles_coming);
    }

    public function getScoreValues(): array {
        return array_values($this->score_values);
    }

    public function getScoreValuesByTurn(): array {
        return $this->score_values;
    }

    public function getScore(): int {
        return array_sum($this->score_values);
    }

    public function increaseScore(int $turn, int $amount): void {
        $this->score_values[$turn] = $amount;
    }

    public function getName(): string {
        return $this->name;
    }

    public function getWantedQwirkleSize(): int {
        return $this->wanted_qwirkle_size;
    }

    public function leaveGame(): void {
        $this->in_game = false;
    }

    public function isOutOfTiles(): bool {
        return $this->hand->isEmpty();
    }

    public function isInGame(): bool {
        return $this->in_game;
    }

    public function getId(): string {
        return $this->id;
    }

    public function getLastSwapCount(): int {
        return $this->hand ? $this->hand->getLastSwapCount() : 0;
    }

    public function getPublicStatus(): array {
        return [
            "id" => $this->getId(),
            "name" => $this->getName(),
            "score" => $this->getScore(),
            "scoreValues" => $this->getScoreValues(),
            "isInGame" => $this->isInGame(),
            "handCount" => $this->hand ? $this->hand->getCount() : 0,
            "lastSwapCount" => $this->getLastSwapCount(),
            "wantedQwirkleSize" => $this->getWantedQwirkleSize()
        ];
    }

    public function __toString() {
        $ret = "Player " . $this->getName() . ", Score " . $this->getScore() . ", InGame " . $this->isInGame() . ", Hand: " . $this->hand;
        return $ret;
    }

}
