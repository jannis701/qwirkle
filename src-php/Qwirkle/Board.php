<?php

namespace Qwirkle;

use Qwirkle\Matrix2D;

class Board {

    private $tiles_map = [];
    private $last_used_tiles_map = [];
    private $qwirkle_size = 0;

    public function __construct(int $qwirkle_size, array $tiles_map_init = []) {
        $this->qwirkle_size = $qwirkle_size;
        $this->tiles_map = $tiles_map_init;
    }

    public function putTile(int $x, int $y, int $color, int $shape): void {
        if (isset($this->tiles_map[$x][$y])) {
            throw new \ErrorException("Cannot put at cordinate ($x,$y) -- already used");
        }
        if ($color < 0 || $color >= $this->qwirkle_size || $shape < 0 || $shape >= $this->qwirkle_size) {
            throw new \InvalidArgumentException("Invalid tile ($color,$shape)");
        }
        $this->tiles_map[$x][$y] = [$color, $shape];
        $this->last_used_tiles_map[$x][$y] = [$color, $shape];
    }

    public function putTiles(array $played_tiles): void {
        $this->last_used_tiles_map = [];
        foreach ($played_tiles as $x => $data) {
            if (!is_array($data)) {
                throw new \InvalidArgumentException("Invalid data for played tiles.");
            }
            foreach ($data as $y => $value) {
                list ($color, $shape) = $value;
                $this->putTile(intval($x), intval($y), intval($color), intval($shape));
            }
        }
    }

    public function validateMoveAndGetScore($is_first_move = false): int {
        $last_used_cordinates = [];
        foreach ($this->last_used_tiles_map as $x => $data) {
            foreach ($data as $y => $tile) {
                $last_used_cordinates[] = [$x, $y];
            }
        }
        $move_map = new Matrix2D($last_used_cordinates);
        if (!$this->hasNoGap($move_map)) {
            throw new \ErrorException("Invalid position for tiles.");
        }
        if ($is_first_move && count($last_used_cordinates) === 1) {
            return 1;
        }
        return $this->calculateScoreByMove($move_map, $is_first_move);
    }

    /**
     * identify all rows and lines of tiles connected to the elements of 
     * $move_map-cordinates and sum up their score
     * @param Matrix2D $move_map
     * @param bool $ignore_connection don't check connection to existing tiles (important for first move)
     * @return int
     */
    public function calculateScoreByMove(Matrix2D $move_map, bool $ignore_connection = false): int {
        $score = 0;
        $is_connected = $ignore_connection;
        $line_direction = $move_map->getLineDirection();
        switch ($line_direction) {
            case Matrix2D::IS_COLUMN:
                // the score of the tiles in the played column
                $vertical_tiles = $this->getVerticalTilesConnected($move_map->getXmin(), $move_map->getYmin());
                $score += $this->calculateScoreByTiles($vertical_tiles);
                if (count($move_map->getValuesY()) < count($vertical_tiles)) {
                    $is_connected = true;
                }

                // the score of the tiles in all rows which are connected to the played tiles
                foreach ($move_map->getValuesY() as $y) {
                    $horizontal_tiles = $this->getHorizontalTilesConnected($move_map->getXmin(), $y);
                    $score += $this->calculateScoreByTiles($horizontal_tiles);
                    if (count($horizontal_tiles) > 1) {
                        $is_connected = true;
                    }
                }
                break;
            case Matrix2D::IS_ROW:
            case Matrix2D::IS_SINGLE:
                // the score of the tiles in the played line
                $horizontal_tiles = $this->getHorizontalTilesConnected($move_map->getXmin(), $move_map->getYmin());
                $score += $this->calculateScoreByTiles($horizontal_tiles);
                if (count($move_map->getValuesX()) < count($horizontal_tiles)) {
                    $is_connected = true;
                }

                // the score of the tiles in all lines which are connected to the played tiles
                foreach ($move_map->getValuesX() as $x) {
                    $vertical_tiles = $this->getVerticalTilesConnected($x, $move_map->getYmin());
                    $score += $this->calculateScoreByTiles($vertical_tiles);
                    if (count($vertical_tiles) > 1) {
                        $is_connected = true;
                    }
                }
                break;
            default:
                return 0;
        }
        if (!$is_connected) {
            throw new \ErrorException("Invalid position for tiles.");
        }
        return $score;
    }

    /**
     * calculate the score of connected tiles. throw exception, if invalid combination
     * of tiles is detected
     * @param array $tiles
     * @return int
     * @throws \ErrorException
     */
    public function calculateScoreByTiles($tiles) {
        $tiles_map = new Matrix2D($tiles);
        if ($tiles_map->getLineDirection() === Matrix2D::IS_NO_LINE || !$tiles_map->elementsAreUnique()) {
            throw new \ErrorException("Invalid combination of tiles.");
        }

        $score = count($tiles);
        if ($score === 1) {
            return 0;
        }
        if ($score === $this->qwirkle_size) {
            $score *= 2;
        }
        return $score;
    }

    public function getVerticalTilesConnected(int $x, int $y): array {
        $tiles = [$this->tiles_map[$x][$y]];
        $d = 1;
        while (isset($this->tiles_map[$x][$y + $d])) {
            $tiles[] = $this->tiles_map[$x][$y + $d];
            $d++;
        }
        $d = 1;
        while (isset($this->tiles_map[$x][$y - $d])) {
            $tiles[] = $this->tiles_map[$x][$y - $d];
            $d++;
        }
        return $tiles;
    }

    public function getHorizontalTilesConnected(int $x, int $y): array {
        $tiles = [$this->tiles_map[$x][$y]];
        $d = 1;
        while (isset($this->tiles_map[$x + $d][$y])) {
            $tiles[] = $this->tiles_map[$x + $d][$y];
            $d++;
        }
        $d = 1;
        while (isset($this->tiles_map[$x - $d][$y])) {
            $tiles[] = $this->tiles_map[$x - $d][$y];
            $d++;
        }
        return $tiles;
    }

    /**
     * check if elements (cordinates) of $move_map are connected themselves and
     * connected to the existing tiles on the board (the latter only if 
     * $ignoreConnection is false)
     * @param Matrix2D $move_map
     * @return bool
     */
    public function hasNoGap(Matrix2D $move_map): bool {
        $line_direction = $move_map->getLineDirection();
        switch ($line_direction) {
            case Matrix2D::IS_COLUMN:
                $x = $move_map->getXmin();
                // before our first or after our last tile is already an existing tile
                for ($y = $move_map->getYmin(); $y <= $move_map->getYmax(); $y++) {
                    if (!isset($this->tiles_map[$x][$y])) {
                        return false;
                    }
                }
                break;
            case Matrix2D::IS_SINGLE:
                break;
            case Matrix2D::IS_ROW:
                $y = $move_map->getYmin();
                // before our first or after our last tile is already an existing tile
                for ($x = $move_map->getXmin(); $x <= $move_map->getXmax(); $x++) {
                    if (!isset($this->tiles_map[$x][$y])) {
                        return false;
                    }
                }
                break;
            default:
                return false;
        }
        return true;
    }

    public function getLastUsedTilesByCordinate(): array {
        return $this->last_used_tiles_map;
    }

    public function getLastUsedTiles(): array {
        $last_used_tiles = [];
        foreach ($this->last_used_tiles_map as $x => $data) {
            foreach ($data as $y => $tile) {
                $last_used_tiles[] = $tile;
            }
        }
        return $last_used_tiles;
    }

    public function copy(): Board {
        $board = new Board($this->qwirkle_size, $this->tiles_map);
        return $board;
    }

    public function isEmpty(): bool {
        return empty($this->tiles_map);
    }

    public function getAllTiles(): array {
        return $this->tiles_map;
    }

    public function getAllTilesDebug(): array {
        $debug_tiles = [];
        for ($i = 0; $i < 10; $i++) {
            for ($j = 0; $j < 10; $j++) {
                $debug_tiles[$i][$j] = [$i, $j];
            }
        }
        return $debug_tiles;
    }

    public function __toString() {
        return print_r($this->tiles_map, true);
    }

}
