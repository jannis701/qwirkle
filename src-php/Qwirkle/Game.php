<?php

namespace Qwirkle;

class Game {

    private $game_id = "";
    private $players = [];
    private $player_order = [];
    private $current_player = null;
    private $deck;
    private $board;
    private $moves = [];
    private $turn = 0;
    private $state;
    private $qwirkle_size = 0;

    const STATUS_LOBBY = 0;
    const STATUS_RUNNING = 1;
    const STATUS_FINISHED = 2;

    public function __construct(string $game_id) {
        $this->game_id = $game_id;

        $this->turn = 0;
        $this->players = [];
        $this->state = self::STATUS_LOBBY;
    }

    public function getId(): string {
        return $this->game_id;
    }

    public static function sanitizeGameId(string $id) {
        return preg_replace("/[^a-z0-9]/", "", strtolower($id));
    }

    /**
     * 
     * @param array $tiles : [ 0 => [c1,s1], 1 => [c2,s2], ...], cX = color, sX = shape
     * @param string $player_id
     * @return void
     * @throws \ErrorException
     */
    public function swapTiles(array $tiles, string $player_id): void {
        if ($this->state !== self::STATUS_RUNNING) {
            throw new \ErrorException("Playing is not possible. The game is currently not running.");
        }

        $p = $this->getPlayerById($player_id);
        if (!$p->hasTiles($tiles)) {
            throw new \ErrorException("The Player does not have the chosen tiles in the hand.");
        }

        $number_of_tiles = count($tiles);

        if ($number_of_tiles === 0) {
            if ($this->deck->remaining() > 0) {
                throw new \ErrorException("No tile selected. Swap at least one tile.");
            } else {
                $p->leaveGame();
                $this->nextTurn();
            }
        }

        $new_tiles = $this->deck->drawTiles($number_of_tiles, true);
        $p->swapTiles($tiles, $new_tiles);
        $p->increaseScore($this->turn, 0);
        $this->deck->addTiles($tiles);
        $this->board->putTiles([]); // empty last_used_cordinates
        $this->nextTurn();
    }

    public function getPlayerCount() {
        return count($this->players);
    }

    public function getMaxScorePlayerId() {
        $max_score = 0;
        $player_id = null;
        foreach ($this->players as $player) {
            $current_score = $player->getScore();
            if ($current_score > $max_score) {
                $max_score = $current_score;
                $player_id = $player->getId();
            }
        }
        return $player_id;
    }

    public function startGame() {
        $this->ensureState(self::STATUS_LOBBY, "Starting game is not possible. The lobby is closed.");
        $player_count = $this->getPlayerCount();
        if ($player_count <= 1) {
            throw new \ErrorException("Starting game is not possible. Not enough players.");
        }

        $this->qwirkle_size = 0;
        foreach ($this->players as $player_id => $player) {
            if ($this->qwirkle_size === 0) {
                $this->qwirkle_size = $player->getWantedQwirkleSize();
            } else if ($this->qwirkle_size !== $player->getWantedQwirkleSize()) {
                throw new \ErrorException("Starting game is not possible. Players do not agree on Qwirkle size.");
            }
        }

        $this->deck = new Deck($this->qwirkle_size);
        $this->board = new Board($this->qwirkle_size);
        $this->moves = [];

        $this->state = self::STATUS_RUNNING;

        $max_points_for_first_move = 0;
        foreach ($this->players as $player_id => $player) {
            $initial_tiles = $this->deck->drawTiles($this->qwirkle_size);
            $hand = new Hand($initial_tiles);
            $player->setHand($hand);
            $hand_value = $hand->getHandValue();
            if ($hand_value > $max_points_for_first_move) {
                $this->current_player = $player_id;
                $max_points_for_first_move = $hand_value;
            }
        }

        // put start-player at the beginning of the $player_order
        $player_ids = array_keys($this->players);
        $player_ids = array_diff($player_ids, [$this->current_player]);
        array_unshift($player_ids, $this->current_player);
        for ($i = 0; $i < $player_count; $i++) {
            $this->player_order[$player_ids[$i]] = $player_ids[($i + 1) % $player_count];
        }
    }

    public function finishGame() {
        $this->ensureState(self::STATUS_RUNNING, "Finishing game is not possible. The Game is not running.");
        $this->state = self::STATUS_FINISHED;
    }

    /**
     * 
     * @param array $tbc tiles by cordinate: tbc[x][y]=[c,s] -- x,y: cordinates, c: color, s: shape
     * @param string $player_id
     * @return void
     * @throws \ErrorException
     */
    public function playTiles(array $tbc, string $player_id): void {
        $this->ensureState(self::STATUS_RUNNING, "Playing is not possible. The game is currently not running.");

        if ($this->current_player !== $player_id) {
            throw new \ErrorException("Player is not on turn.");
        }

        $p = $this->getPlayerById($player_id);

        $tmp_board = $this->board->copy();
        $tmp_board->putTiles($tbc);

        $last_used_tiles = $tmp_board->getLastUsedTiles();

        if (!$p->hasTiles($last_used_tiles)) {
            throw new \ErrorException("The Player does not have the chosen tiles in the hand.");
        }

        // only if the board was empty BEFORE putTiles (therefore do NOT use the copy here)
        $first_tile_on_board = $this->board->isEmpty();
        // getscore also checks for validity of the move, therefore use the copy
        // before putting tiles on real board
        $move_score = $tmp_board->validateMoveAndGetScore($first_tile_on_board);
        $p->increaseScore($this->turn, $move_score);

        $this->board->putTiles($tbc);

        $new_tiles = $this->deck->drawTiles(count($last_used_tiles));

        $p->swapTiles($last_used_tiles, $new_tiles);

        if ($this->deck->remaining() === 0 && $p->isOutOfTiles()) {
            $p->increaseScore($this->turn, $move_score + $this->qwirkle_size);
            $this->finishGame();
            return;
        }

        $this->nextTurn();
    }

    public function nextTurn(): void {
        $this->current_player = $this->player_order[$this->current_player];
        $this->turn++;
        $skip_count = 0;
        while (!$this->players[$this->current_player]->isInGame()) {
            $this->current_player = $this->player_order[$this->current_player];
            $this->turn++;
            $skip_count++;
            if ($skip_count >= $this->getPlayerCount()) {
                $this->finishGame();
                break;
            }
        }
    }

    public function getStatus(string $player_id): array {
        $current_player = $this->players[$player_id] ?? null;
        $all_players = [];
        foreach ($this->player_order ?: $this->players as $p_id => $p_data) {
            $all_players[] = $this->players[$p_id]->getPublicStatus();
        }
        $ret = [
            "gameState" => $this->state,
            "turn" => $this->turn,
            "playerOnTurn" => $this->current_player,
            "playerMaxScore" => $this->getMaxScorePlayerId(),
            "all_players" => $all_players
        ];
        if ($this->board && $this->deck) {
            $ret["qwirkleSize"] = $this->qwirkle_size;
            $ret["boardTiles"] = $this->board->getAllTiles();
            $ret["lastUsedTiles"] = $this->board->getLastUsedTilesByCordinate();
            $ret["remainingTilesCount"] = $this->deck->remaining();
        }
        if ($current_player) {
            $ret["player"]["isOnTurn"] = $current_player->getId() === $this->current_player;
            $ret["player"]["hand"] = $current_player->getHand();
            $ret["player"]["name"] = $current_player->getName();
            $ret["player"]["lastSwapCount"] = $current_player->getLastSwapCount();
            $ret["player"]["player_id"] = $player_id;
        }
        return $ret;
    }

    public function getStatusHash() {
        return md5($this->__toString());
    }

    public function getPlayerById(string $player_id): Player {
        if (!isset($this->players[$player_id])) {
            throw new \InvalidArgumentException("Player with ID " . $player_id . " does not exist.");
        }
        return $this->players[$player_id];
    }

    public function addPlayer(Player $p): string {
        $this->ensureState(self::STATUS_LOBBY, "Adding player not possible. The Lobby is already closed.");
        $player_id = $p->getId();
        if (isset($this->players[$player_id])) {
            throw new \InvalidArgumentException("Player with Name " . $p->getName() . " already exists");
        }
        $this->players[$player_id] = $p;
        return $player_id;
    }

    public function removePlayer(string $player_id): void {
        if (!isset($this->players[$player_id])) {
            return;
        }
        switch ($this->state) {
            case self::STATUS_LOBBY:
                unset($this->players[$player_id]);
                break;
            case self::STATUS_RUNNING:
                $this->players[$player_id]->leaveGame();
                if ($this->current_player === $player_id) {
                    $this->nextTurn();
                }
                break;
            default:
                throw new \ErrorException("Removing player not possible. Invalid status.");
        }
    }

    public function ensureState(int $state, string $msg): void {
        if ($this->state !== $state) {
            throw new \ErrorException($msg);
        }
    }

    public function __toString() {
        $ret = "Game " . $this->game_id . "\n\n";
        $ret .= "Players:\n";
        foreach ($this->players as $player) {
            $ret .= $player . "\n";
        }
        if ($this->current_player) {
            $ret .= "current: " . $this->players[$this->current_player]->getName();
        }
        $ret .= "\nStatus: " . $this->state;
        $ret .= $this->board;
        return $ret;
    }

}
