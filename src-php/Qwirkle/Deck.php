<?php

namespace Qwirkle;

class Deck {

    private $tiles = [];

    /**
     * Creates a new Deck which is filled with all possible tiles combinations, three times.
     */
    public function __construct($qwirkle_size) {
        $this->tiles = [];

        for ($color = 0; $color < $qwirkle_size; $color++) {
            for ($shape = 0; $shape < $qwirkle_size; $shape++) {
                for ($i = 0; $i < 3; $i++) {
                    $this->tiles[] = [$color, $shape];
                }
            }
        }
        $this->shuffle();
    }

    /**
     * Shuffles the Deck in a random order.
     */
    public function shuffle() {
        shuffle($this->tiles);
    }

    public function remaining(): int {
        return count($this->tiles);
    }

    public function drawTiles($count, $exactcount = false): array {
        if ($this->remaining() < $count) {
            if ($exactcount) {
                throw new \ErrorException("Not enough tiles left to swap.");
            }
            $count = $this->remaining();
        }
        $tiles = [];
        for ($i = 0; $i < $count; $i++) {
            $tiles[] = array_pop($this->tiles);
        }
        return $tiles;
    }

    public function addTiles($tiles) {
        foreach ($tiles as $t) {
            $this->tiles[] = $t;
        }
        $this->shuffle();
    }

    public function __toString() {
        return print_r($this->tiles, true);
    }

}
