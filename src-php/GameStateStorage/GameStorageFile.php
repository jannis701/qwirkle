<?php

namespace GameStateStorage;

class GameStorageFile implements GameStorage {

    private $path = "";
    private $fo = null;
    private $locked = false;

    public function __construct(string $game_id, array $options) {
        $path = $options["path"] ?? "";
        if (!$path || !is_dir($path)) {
            throw new \InvalidArgumentException("$path is not a directory.");
        }

        $game_id_sanitized = self::sanitizeGameId($game_id);
        $this->path = $path . DIRECTORY_SEPARATOR . $game_id_sanitized;
    }

    public static function sanitizeGameId(string $id): string {
        $game_id_sanitized = preg_replace("/[^a-z0-9]/", "", strtolower($id));
        if (!$game_id_sanitized) {
            throw new \ErrorException("Invalid game ID.");
        }
        return $game_id_sanitized;
    }

    public function lockGameData(): void {
//        $this->fo = fopen($this->path, 'c+');
//        $lock = flock($this->fo, LOCK_EX);
//        if (!$lock) {
//            throw new \ErrorException('Cannot obtain lock for file "' . $this->path);
//        }
        $this->locked = true;
    }

    public function unlockGameData(): void {
//        flock($this->fo, LOCK_UN);
        $this->locked = false;
    }

    public function existsGameData(): bool {
        return file_exists($this->path) && is_readable($this->path) && is_writable($this->path);
    }

    public function readGameData(): string {
        if (!$this->existsGameData()) {
            throw new \ErrorException("No read-/writeable data found.");
        }

//        if (!$this->locked) {
//            $this->fo = fopen($this->path, 'r');
//            $lock = flock($this->fo, LOCK_SH | LOCK_NB);
//
//            if (!$lock) {
//                throw new \ErrorException('Cannot obtain lock for file "' . $this->path);
//            }
//        }

        $content = file_get_contents($this->path);

//        if (!$this->locked) {
//            flock($this->fo, LOCK_UN);
//            fclose($this->fo);
//        }

        return $content;
    }

    public function writeGameData(string $content): void {
        if (!$this->locked) {
            throw new \ErrorException("File not locked for writing. Please use lockGameData()");
        }

        file_put_contents($this->path, $content);
    }

}
