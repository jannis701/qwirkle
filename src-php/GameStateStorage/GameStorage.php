<?php

namespace GameStateStorage;

interface GameStorage {

    public function __construct(string $game_id, array $options);

    public function existsGameData(): bool;

    public function readGameData(): string;

    public function lockGameData(): void;

    public function unlockGameData(): void;

    public function writeGameData(string $game): void;
}
