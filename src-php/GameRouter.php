<?php

use Qwirkle\Game;
use Qwirkle\Player;
use GameStateStorage\GameStorageFile;

class GameRouter {

    const STORAGE_TYPE_FILE = 0;

    /**
     * 
     * @var Game
     */
    private $game;
    private $data_path;
    private $write_required = ["actionaddplayer" => true, "actionremoveplayer" => true, "actionleavegame" => true, "actionresetgame" => true, "actionstartgame" => true, "actioncreategame" => true, "actionplayturn" => true, "actionswapturn" => true];

    public function __construct(string $data_path) {
        $this->data_path = $data_path;
    }

    public static function getGameStorage(string $game_id, int $storage_type, array $options = []): GameStorageFile {
        switch ($storage_type) {
            case self::STORAGE_TYPE_FILE:
                return new GameStorageFile($game_id, $options);
            default:
                throw new \InvalidArgumentException("Unkown storage-type: $storage_type");
        }
    }

    public static function unserializeGame(string $content): Game {
        if (!$content || !is_string($content)) {
            throw new \ErrorException("Invalid data content.");
        }

        $game = unserialize($content, ["allowed_classes" => ["Qwirkle\Game", "Qwirkle\Player", "Qwirkle\Board", "Qwirkle\Deck", "Qwirkle\Point2D", "Qwirkle\Hand", "Qwirkle\Matrix2D"]]);
        if ($game === false || get_class($game) !== 'Qwirkle\Game') {
            throw new \ErrorException("Invalid data content unserialized.");
        }
        return $game;
    }

    public static function serializeGame(Game $game): string {
        return serialize($game);
    }

    public static function getSanitizedArgument(string $arg): string {
        /*
          // angular $http.post()-paramater auslesen und ggfs. in $_POST-Variable schreiben.
          // ACHTUNG: die daten sind nicht über filter_input() oder $_REQUEST zugänglich!
          if (empty($_POST)) {
          $post_data = file_get_contents('php://input');
          if ($post_data) {
          $_POST = json_decode($post_data, TRUE);
          }
          }
         */
        return filter_input(INPUT_POST, $arg, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_BACKTICK) ?:
                filter_input(INPUT_GET, $arg, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH | FILTER_FLAG_STRIP_BACKTICK) ?:
                $_SESSION[$arg] ?? "";
    }

    public static function sanitizeId(string $id): string {
        $id_sanitized = preg_replace("/[^a-z0-9]/", "", strtolower($id));
        if (!$id_sanitized) {
            throw new ErrorException("Invalid ID.", 400);
        }
        return $id_sanitized;
    }

    public static function jsonToArray(string $json_string): array {
        $tiles_by_cordinate = json_decode($json_string, true, 1, JSON_THROW_ON_ERROR);
        if (!is_array($tiles_by_cordinate)) {
            throw new ErrorException("Invalid data for move.", 400);
        }
        foreach ($tiles_by_cordinate as $key => $value) {
            if (intval($key) != $key || intval($value) != $value) {
                throw new ErrorException("Invalid data for move.", 400);
            }
        }
        return $tiles_by_cordinate;
    }

    public function runActionJson(string $game_id, string $cmd) {
        header('Content-Type: application/json');
        // DEBUG
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Allow-Origin: http://localhost:8383');

        try {
            print json_encode($this->runAction($game_id, $cmd));
        } catch (\Exception $ex) {
            $error_code = $ex->getCode();
            if ($error_code !== 0) {
                http_response_code($error_code);
            }
            $ret = ["error" => ["code" => $error_code, "msg" => $ex->getMessage(), "game_id" => $_SESSION["game_id"] ?? null]];
            print json_encode($ret);
        }
    }

    public function runAction(string $game_id, string $cmd) {
        try {
            $method_name = 'action' . self::sanitizeId($cmd);
            $game_id_sanitized = self::sanitizeId($game_id);
            if (!method_exists($this, $method_name)) {
                throw new ErrorException("Invalid command.", 400);
            }
        } catch (\Exception $ex) {
            throw new ErrorException("Command oder Spiel nicht gefunden.", 404);
            return;
        }

        $_SESSION["game_id"] = $game_id_sanitized;

        $method = new ReflectionMethod($this, $method_name);

        $params = $this->getParams($method);

        $write_required = $this->write_required[$method_name] ?? false;
        $storage = self::getGameStorage($game_id_sanitized, self::STORAGE_TYPE_FILE, ["path" => $this->data_path]);
        if ($write_required) {
            $storage->lockGameData();
        }
        $this->game = $storage->existsGameData() ? self::unserializeGame($storage->readGameData()) : new Game($game_id_sanitized);

        try {
            $ret = $method->invokeArgs($this, $params);
        } catch (Exception $e) {
            if ($write_required) {
                $storage->unlockGameData();
            }
            throw $e;
        }
        if ($write_required) {
            $game_data = self::serializeGame($this->game);
            $storage->writeGameData($game_data);
            $storage->unlockGameData();
        }

        return $ret;
    }

    public function getParam(string $name) {
        return $_POST[$name] ?? $_GET[$name] ?? $_SESSION[$name] ?? null;
    }

    public function getParams($method) {
        $ret = [];
        foreach ($method->getParameters() as $i => $param) {
            $name = $param->getName();
            $value = $this->getParam($name);
            if ($value !== null) {
                if (version_compare(PHP_VERSION, '8.0', '>=')) {
                    $method_param_is_array = $param->getType() && $param->getType()->getName() === 'array';
                } else {
                    $method_param_is_array = $param->isArray();
                }

                if ($method_param_is_array) {
                    $ret[] = is_array($value) ? $value : array($value);
                } elseif (!is_array($value)) {
                    $ret[] = $value;
                } else {
                    throw new ErrorException("Invalid parameter for method.", 400);
                }
            } elseif ($param->isDefaultValueAvailable()) {
                $ret[] = $param->getDefaultValue();
            } else {
                throw new ErrorException("Invalid parameter for method.", 400);
            }
        }
        return $ret;
    }

    public function actionShowGame() {
        return ["success" => ["msg" => $this->game->__toString()]];
    }

    public function actionGameStatus(string $force = "false") {
        $status_hash = $this->game->getStatusHash();

        $changed = ($force == "true" || !isset($_SESSION["last_status_hash"]) || $status_hash !== $_SESSION["last_status_hash"]);

        $ret = ["success" => ["msg" => "Output for game status", "changed" => $changed]];
        if ($changed) {
            $_SESSION["last_status_hash"] = $status_hash;
            $ret["success"]["data"] = $this->game->getStatus($_SESSION["player_id"] ?? "");
        }
        return $ret;
    }

    public function actionCreateGame() {
        return ["success" => ["msg" => "Created game."]];
    }

    public function actionResetGame() {
        $this->game->ensureState(Game::STATUS_FINISHED, "Reset game is not possible. The Game is running.");
        $this->game = new Game($this->game->getId());
        return ["success" => ["msg" => "Reseted the game."]];
    }

    public function actionStartGame() {
        $this->game->startGame();
        return ["success" => ["msg" => "Started the game."]];
    }

    public function actionAddPlayer(string $name, string $qwirkle_size) {
        try {
            $p = $this->game->getPlayerById($_SESSION["player_id"] ?? "");
        } catch (\Exception $ex) {
            $p = new Player($name, intval($qwirkle_size));
            $this->game->addPlayer($p);
            $_SESSION["player_id"] = $p->getId();
        }

        return ["success" => ["msg" => "Added Player " . $p->getName(), "player_id" => $p->getId()]];
    }

    public function actionLeaveGame() {
        $player_id = $_SESSION["player_id"];
        $this->game->removePlayer($_SESSION["player_id"] ?? "");
        unset($_SESSION["player_id"], $_SESSION["game_id"]);
        return ["success" => ["msg" => "Removed Player with ID $player_id"]];
    }

    public function actionRemovePlayer(string $player_id) {
        if (!isset($_SESSION["player_id"]) || $_SESSION["player_id"] !== $player_id) {
            throw new ErrorException("You are not allowed to remove this player.");
        }
        $this->game->removePlayer($player_id);
        unset($_SESSION["player_id"]);
        return ["success" => ["msg" => "Removed Player with ID $player_id"]];
    }

    public function actionPlayTurn(array $tiles_by_cordinate) {
        // $tiles_by_cordinate_string = self::getSanitizedArgument("tiles_by_cordinate");
        // $tiles_by_cordinate = self::jsonToArray($tiles_by_cordinate_string);
        $tbc = [];
        foreach ($tiles_by_cordinate as $x => $data) {
            foreach ($data as $y => $tile_string) {
                $tbc[$x][$y] = explode("_", $tile_string, 2);
            }
        }
        $this->game->playTiles($tbc, $_SESSION["player_id"] ?? "");
        return ["success" => ["msg" => "Played Turn sucessfully."]];
    }

    public function actionSwapTurn(array $tiles) {
        $ts = [];
        foreach ($tiles as $tile) {
            $ts[] = explode("_", $tile, 2);
        }
        $this->game->swapTiles($ts, $_SESSION["player_id"] ?? "");
        return ["success" => ["msg" => "Played Turn sucessfully."]];
    }

}
