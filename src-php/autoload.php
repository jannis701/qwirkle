<?php

/**
 * autoload.php
 * setup PHP autoload. Include this file to automatically load 
 * classes in lib with the filename CLASSNAME.class.php
 */

// Autoload-Implementation that loads classes case-sensitive
spl_autoload_register(function ($class) {
    /**
     * Default: load classes in lib-Folder by namespace-class-path
     */
    // transform class-path to file-path
    $class_path = str_replace('\\', DIRECTORY_SEPARATOR, $class);
    $path = __DIR__ . DIRECTORY_SEPARATOR . $class_path . '.php';

    if (file_exists($path)) {
        require $path;
    }
});
