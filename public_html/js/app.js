import i18n from "./i18n.js";
import { localeData } from "./i18n/de-DE.js"; // default locale
import { loadElement, loadTemplate, redrawElement, createElem, modifyElem, encodeTile } from "./utils.js";

var configData = {
    "backendBaseUrl": "cmd.php"
};

document.addEventListener("DOMContentLoaded", function (event) {

    document.getElementById("noscript").classList.add("hidden");
    if (navigator.cookieEnabled) {
        document.getElementById("nocookies").classList.add("hidden");
    }
    let searchParams = new URLSearchParams(window.location.search);
    let locale = searchParams.get("lang");
    setI18n(locale).then(() => {
        const game_id = window.location.hash ? window.location.hash.substr(1) : null;

        // todo: player-name fragen
        var game = new Game("game");
        game.start(game_id);
    });

});

function setI18n(locale) {
    let valid_locales = {"de-DE": true, "en-US": true};
    if (locale && valid_locales.hasOwnProperty(locale)) {
        const localeFile = "./i18n/" + locale + ".js";
        return import(localeFile).then((languageData) => {
            return i18n.translator.add(languageData.localeData);
        })
                .catch((error) => {
                    console.log("Error loading locale from " + localeFile + ". Using default locale!");
                    return i18n.translator.add(localeData);
                });
    } else {
        return new Promise(resolve => {
            resolve(i18n.translator.add(localeData));
        });
    }
}

class Game {
    constructor(element) {
        this._httpClient = new GameHttpClient();
        this._lobbyDrawer = new LobbyDrawer(element);
        this._gameDrawer = new GameDrawer(element);
        this._resultDrawer = new ResultDrawer(element);
        this._gameStatus = null;
        this._lastGameState = null;
        this._gameTimer = null;
    }

    start(gameId = null) {
        // no gameId or invalid gameId
        if (!gameId || gameId.search(/[^a-z0-9A-Z]/) !== -1) {
            this._gameId = this.generateRandomId();
        } else {
            this._gameId = gameId;
        }
        this._httpClient.createGame(this._gameId).then(() => {
            window.location.hash = "#" + this._gameId;
            return this.updateStatus(true)
        })
                .then(() => this.startTimer())
                .catch(e => alert(i18n(e.message)));
    }

    draw() {
        if (this._lastGameState !== this._gameStatus.gameState) {
            switch (this._gameStatus.gameState) {
                case 0:
                    this._lobbyDrawer.draw(
                            (userName, qwirkleSize) => {
                        this.joinGame(userName, qwirkleSize)
                    }, () => {
                        this.leaveGame();
                    }, () => {
                        this.startGame();
                    }).then(() => this._lobbyDrawer.refresh(this._gameStatus.all_players, this._gameStatus.player, this._gameId));
                    break;
                case 1:
                    this._gameDrawer.draw(
                            (force) => {
                        this.updateStatus(force);
                    },
                            (playedElements) => {
                        this.play(playedElements);
                    },
                            (swappedElements) => {
                        this.swap(swappedElements);
                    },
                            () => {
                        this.draw();
                    },
                            this._gameStatus.qwirkleSize
                            ).then(() => this._gameDrawer.refresh(this._gameStatus));
                    break;
                case 2:
                    alert(i18n("gameFinished"));
                    this._resultDrawer.draw(() => {
                        this.reset();
                    },
                            () => {
                        this.draw();
                    },
                            this._gameStatus.qwirkleSize
                            ).then(() => this._resultDrawer.refresh(this._gameStatus));
                    break;
                default:
                    throw new Error("Invalid game-state reached :(");
            }
            this._lastGameState = this._gameStatus.gameState;
            return;
        }

        switch (this._gameStatus.gameState) {
            case 0:
                this._lobbyDrawer.refresh(this._gameStatus.all_players, this._gameStatus.player);
                break;
            case 1:
                this._gameDrawer.refresh(this._gameStatus);
                break;
            case 2:
                this._resultDrawer.refresh(this._gameStatus);
                break;
            default:
                throw new Error("Invalid game-state reached :(");
        }
    }

    joinGame(userName, qwirkleSize) {
        this._httpClient.addPlayer(userName, qwirkleSize)
                .then((response) => {
                    this.updateStatus(true)
                })
                .catch(e => alert(i18n(e.message)));
    }

    leaveGame() {
        this._httpClient.leaveGame().then((json_data) => {
            return this.updateStatus(true);
        })
                .catch(e => alert(i18n(e.message)));
    }

    startGame() {
        this._httpClient.startGame().catch(e => alert(i18n(e.message)));
    }

    startTimer() {
        var currentGame = this;
        this._gameTimer = window.setInterval(function () {
            currentGame.updateStatus(false);
        }, 2600);
    }

    stopTimer() {
        clearInterval(this._gameTimer);
    }

    reset() {
        return this._httpClient.resetGame().then(
                () => this._lobbyDrawer.draw(this._gameStatus.all_players, this._gameStatus.player, this._gameId))
                .catch(e => alert(i18n(e.message)));
    }

    play(playedElements) {
        if (playedElements.length === 0) {
            return;
        }
        var params = {};
        for (var i = 0; i < playedElements.length; i++) {
            let paramName = "tiles_by_cordinate[" + playedElements[i].getAttribute("data-field-x") + "][" + playedElements[i].getAttribute("data-field-y") + "]";
            if (typeof params[paramName] !== "undefined") {
                alert(i18n("Invalid position for tiles."));
                return this.updateStatus(true);
            }
            params[paramName] = encodeTile(playedElements[i].getAttribute("data-value-c"), playedElements[i].getAttribute("data-value-s"));
        }
        return this._httpClient.playTilesToBoard(params).catch(e => alert(i18n(e.message)));
    }

    swap(swappedElements) {
        if (swappedElements.length === 0) {
            return;
        }
        var tiles = [];
        for (var i = 0; i < swappedElements.length; i++) {
            tiles.push(encodeTile(swappedElements[i].getAttribute("data-value-c"), swappedElements[i].getAttribute("data-value-s")));
        }
        return this._httpClient.swapTiles(tiles).catch(e => alert(i18n(e.message)));
    }

    updateStatus(force) {
        return this._httpClient.getStatusFromServer(this._gameId, force).then((json_data) => {
            if (json_data.success.changed) {
                this._gameStatus = json_data.success.data;
                this.draw();
            }
            return json_data.success.changed;
        });
    }

    generateRandomId() {
        return Math.floor(Math.random() * 100000000000).toString(32);
    }
}

class GameHttpClient {
    getStatusFromServer(gameId, force) {
        const gameStatusUrl = configData.backendBaseUrl + "?cmd=gameStatus&force=" + force + (gameId ? "&game_id=" + gameId : "");
        return fetch(gameStatusUrl, {credentials: "include"}).then(response => response.json()).then(jsonResponse => this.checkForError(jsonResponse));
    }

    createGame(gameId) {
        const url = configData.backendBaseUrl + "?cmd=createGame&game_id=" + gameId;
        return fetch(url, {credentials: "include"}).then(response => response.json()).then(jsonResponse => this.checkForError(jsonResponse));
    }

    addPlayer(userName, qwirkleSize) {
        const url = configData.backendBaseUrl + "?cmd=addPlayer&name=" + userName + "&qwirkle_size=" + qwirkleSize;
        return fetch(url, {credentials: "include"}).then(response => response.json()).then(jsonResponse => this.checkForError(jsonResponse));
    }

    leaveGame() {
        const url = configData.backendBaseUrl + "?cmd=leaveGame";
        return fetch(url, {credentials: "include"}).then(response => response.json()).then(jsonResponse => this.checkForError(jsonResponse));
    }

    startGame() {
        const url = configData.backendBaseUrl + "?cmd=startGame";
        return fetch(url, {credentials: "include"}).then(response => response.json()).then(jsonResponse => this.checkForError(jsonResponse));
    }

    resetGame() {
        const url = configData.backendBaseUrl + "?cmd=resetGame";
        return fetch(url, {credentials: "include"}).then(response => response.json()).then(jsonResponse => this.checkForError(jsonResponse));
    }

    playTilesToBoard(params) {
        const url = configData.backendBaseUrl + "?cmd=playTurn&" + (new URLSearchParams(params)).toString();
        return fetch(url, {credentials: "include"}).then(response => response.json()).then(jsonResponse => this.checkForError(jsonResponse));
    }

    swapTiles(tiles) {
        const url = configData.backendBaseUrl + "?cmd=swapTurn&tiles[]=" + tiles.join("&tiles[]=");
        return fetch(url, {credentials: "include"}).then(response => response.json()).then(jsonResponse => this.checkForError(jsonResponse));
    }

    checkForError(jsonResponse) {
        if (jsonResponse.error) {
            throw new Error(jsonResponse.error.msg);
        }
        return jsonResponse;
    }
}


class LobbyDrawer {
    constructor(element) {
        this._invalidCharsExpr = /[^a-z0-9A-Z ]/;
        this._foundPlayer = false;
        this._mainElement = loadElement(element);
    }

    draw(joinGameCallback, leaveGameCallback, startGameCallback) {
        return loadTemplate("lobby").then((htmlDom) => {
            const gameUrl = window.location.origin + window.location.pathname + window.location.hash;
            this._gameIdHeadingElement = modifyElem("share", {}, [], i18n("inviteplayers"), htmlDom);
            this._gameIdHeadingElement = modifyElem("game-url", {"href": gameUrl}, [], gameUrl, htmlDom);
            this._playersHeadingElement = modifyElem("i18n-players", {}, [], i18n("players") + ":", htmlDom);

            this._playersListElement = htmlDom.getElementById("player-list");
            modifyElem("i18n-qs", {}, [], i18n("qwirkleSize"), htmlDom);
            this._qwirkleSizeInput = htmlDom.getElementById("qs-input");
            // text-field for userName:
            this._userNameInput = modifyElem("username-input", {"placeholder": i18n("name")},
                    [], null, htmlDom);
            this._userNameInput.addEventListener("keyup", () => {
                this.validate()
            });
            this._userNameInput.addEventListener("change", () => {
                this.validate()
            });

            // submit-button
            this._formSubmitElement = modifyElem("player-form-submit", {}, [],
                    i18n("join"), htmlDom);
            this._joinForm = htmlDom.getElementById("player-form");
            this._joinForm.addEventListener("submit", (event) => {
                event.preventDefault();
                if (this._foundPlayer) {
                    leaveGameCallback();
                } else {
                    joinGameCallback(this._userNameInput.value, this._qwirkleSizeInput.value);
                }
                return false;
            });

            // start-button
            this._startButtonElement = modifyElem("start-game-button", {}, [],
                    i18n("startGame"), htmlDom);
            this._startButtonElement.addEventListener("click", (event) => {
                startGameCallback();
            });

            modifyElem("lang-label-de", {"href": "?lang=de-DE" + window.location.hash}, [i18n("langId") === "de-DE" ? "font-bold" : null], null, htmlDom);
            modifyElem("lang-label-en", {"href": "?lang=en-US" + window.location.hash}, [i18n("langId") === "en-US" ? "font-bold" : null], null, htmlDom);
            redrawElement(this._mainElement, htmlDom.body);
        });
    }

    refresh(playerList, playerData) {
        var playersListElement = createElem("ul");
        this._foundPlayer = false;
        for (var i = 0; i < playerList.length; i++) {
            var playerElement = createElem("li", {}, ["p-2"], playerList[i].name + " (" + playerList[i].wantedQwirkleSize + ")");
            if (playerData && playerList[i].id === playerData.player_id) {
                this._foundPlayer = true;
                playerElement.classList.add("highlight");
            }
            playersListElement.appendChild(playerElement);
        }

        if (this._foundPlayer) {
            this._userNameInput.setAttribute("disabled", true);
            this._startButtonElement.removeAttribute("disabled");
            this._formSubmitElement.value = i18n("leaveGame");
        } else {
            this.validate();
            this._userNameInput.removeAttribute("disabled");
            this._userNameInput.focus();
            this._startButtonElement.setAttribute("disabled", true);
            this._formSubmitElement.value = i18n("join");
        }

        if (playerList.length >= 2 && playerData) {
            this._startButtonElement.removeAttribute("disabled");
        } else {
            this._startButtonElement.setAttribute("disabled", true);
        }

        redrawElement(this._playersListElement, playersListElement);
    }

    validate() {
        var userNameIsValid = this.validateField(this._userNameInput);
        if (!userNameIsValid) {
            this._formSubmitElement.setAttribute("disabled", true);
        } else {
            this._formSubmitElement.removeAttribute("disabled");
        }
    }

    validateField(element) {
        var fieldIsValid = (!!element.value && (element.value.search(this._invalidCharsExpr) === -1));
        if (!fieldIsValid && element.value) {
            element.classList.add("text-red-600");
        } else {
            element.classList.remove("text-red-600");
        }
        return fieldIsValid;
    }
}

class GameDrawer {
    constructor(element) {
        this._mainElement = loadElement(element);

        this._board = new Board();
        this._player = new Player();
        this._swapField = new SwapField();
        this._showScoreDetail = false;
    }

    draw(updateStatusCallback, playCallback, swapCallback, redrawCallBack, qwirkleSize) {
        return loadTemplate("game").then((htmlDom) => {
            this._board.resetDimensions(qwirkleSize);
            this._boardElement = htmlDom.getElementById("board");
            this._scoreElement = htmlDom.getElementById("score");
            this._handElement = htmlDom.getElementById("hand");
            this._swapElement = htmlDom.getElementById("swap");
            this._playerControlsElement = htmlDom.getElementById("player-controls");

            // show-score-Element
            this._toggleScoreElement = htmlDom.getElementById("toggle-score");
            this._toggleScoreElement.innerHTML = "&#9661;";
            this._toggleScoreElement.addEventListener("click", (event) => {
                this._showScoreDetail = !this._showScoreDetail;
                this._toggleScoreElement.innerHTML = this._showScoreDetail ? "&#9651;" : "&#9661;";
                redrawCallBack();
            });

            // play-button
            this._playButtonElement = modifyElem("button-play", {}, [],
                    i18n("play"), htmlDom);
            this._playButtonElement.addEventListener("click", (event) => {
                playCallback(document.getElementsByClassName("played"));
                updateStatusCallback(false);
            });
            // play-button
            this._swapButtonElement = modifyElem("button-swap", {}, [],
                    i18n("swap"), htmlDom);
            this._swapButtonElement.addEventListener("click", (event) => {
                swapCallback(document.getElementsByClassName("swapped"));
                updateStatusCallback(false);
            });
            redrawElement(this._mainElement, htmlDom.body);
        });
    }

    refresh(status_data) {
        redrawElement(this._boardElement, this._board.getElement(status_data.boardTiles, status_data.lastUsedTiles));
        if (status_data.player) {
            this._handElement.classList.remove("hidden");
            this._playButtonElement.classList.remove("hidden");
            this._swapButtonElement.classList.remove("hidden");
            redrawElement(this._handElement, this._player.getHandElement(status_data.player));
        } else {
            this._handElement.classList.add("hidden");
            this._playButtonElement.classList.add("hidden");
            this._swapButtonElement.classList.add("hidden");
        }

        let score = new Score();
        let ownPlayer = status_data.player ? status_data.player.player_id : null;
        redrawElement(this._scoreElement, score.getElement(status_data.all_players, status_data.playerOnTurn, ownPlayer, this._showScoreDetail));
        this._scoreElement.classList.remove("grid-cols-" + (status_data.all_players.length + (this._showScoreDetail ? 0 : 1)));
        this._scoreElement.classList.add("grid-cols-" + (status_data.all_players.length + (this._showScoreDetail ? 1 : 0)));

        redrawElement(this._swapElement,
                this._swapField.getElement(status_data.remainingTilesCount,
                        (status_data.lastUsedTiles.length === 0 && status_data.turn !== 0)));

        if (!status_data.player.isOnTurn) {
            this._playButtonElement.setAttribute("disabled", true);
            this._swapButtonElement.setAttribute("disabled", true);
        } else {
            this._playButtonElement.removeAttribute("disabled");
            this._swapButtonElement.removeAttribute("disabled");
        }
    }
}

class ResultDrawer {
    constructor(element) {
        this._mainElement = loadElement(element);
        this._board = new Board();
        this._showScoreDetail = true;
    }

    draw(resetCallback, redrawCallBack, qwirkleSize) {
        return loadTemplate("show-result").then((htmlDom) => {
            this._board.resetDimensions(qwirkleSize);
            this._boardElement = htmlDom.getElementById("board");

            this._showScoreDetail = true;
            this._scoreElement = htmlDom.getElementById("score");
            // toggle-score-Element
            this._toggleScoreElement = htmlDom.getElementById("toggle-score");
            this._toggleScoreElement.innerHTML = "&#9651;";
            this._toggleScoreElement.addEventListener("click", (event) => {
                this._showScoreDetail = !this._showScoreDetail;
                this._toggleScoreElement.innerHTML = this._showScoreDetail ? "&#9651;" : "&#9661;";
                redrawCallBack();
            });
            // reset-button
            this._resetButtonElement = modifyElem("button-reset", {}, [],
                    i18n("newGame"), htmlDom);
            this._resetButtonElement.addEventListener("click", (event) => {
                resetCallback();
            });
            redrawElement(this._mainElement, htmlDom.body);
        });
    }

    refresh(status_data) {
        let score = new Score();
        let ownPlayer = status_data.player ? status_data.player.player_id : null;
        redrawElement(this._scoreElement, score.getElement(status_data.all_players, status_data.playerMaxScore, ownPlayer, this._showScoreDetail));
        this._scoreElement.classList.remove("grid-cols-" + (status_data.all_players.length + (this._showScoreDetail ? 0 : 1)));
        this._scoreElement.classList.add("grid-cols-" + (status_data.all_players.length + (this._showScoreDetail ? 1 : 0)));

        redrawElement(this._boardElement, this._board.getElement(status_data.boardTiles, status_data.lastUsedTiles));
    }
}


class Board {
    constructor(qwirkleSize = 0) {
        this.resetDimensions(qwirkleSize);
    }

    resetDimensions(qwirkleSize) {
        this.qwirkleSize = qwirkleSize;
        this.setDimensions(Math.ceil(qwirkleSize / 2), Math.ceil(qwirkleSize / 2));
    }

    setDimensions(x, y) {
        this._minX = -1 * x;
        this._maxX = x;
        this._minY = -1 * y;
        this._maxY = y;
    }

    adjustDimensions(x, y) {
        if (y + this.qwirkleSize - 1 > this._maxY) {
            this._maxY = y + this.qwirkleSize - 1;
            return true;
        } else if (y - this.qwirkleSize + 1 < this._minY) {
            this._minY = y - this.qwirkleSize + 1;
            return true;
        }
        if (x + this.qwirkleSize - 1 > this._maxX) {
            this._maxX = x + this.qwirkleSize - 1;
            return true;
        } else if (x - this.qwirkleSize + 1 < this._minX) {
            this._minX = x - this.qwirkleSize + 1;
            return true;
        }
        return false;
    }

    getElement(boardTiles, lastUsedTiles) {
        var adjustedDimensions = false;
        var newBoard = createElem("div", {}, ["board"]);
        for (var i = this._maxY; i >= this._minY; i--) {
            var divRow = createElem("div", {}, ["boardRow"]);
            for (var j = this._minX; j <= this._maxX; j++) {
                var hasTile = typeof boardTiles[i] !== "undefined" && typeof boardTiles[i][j] !== "undefined";
                var lastUsed = typeof lastUsedTiles[i] !== "undefined" && typeof lastUsedTiles[i][j] !== "undefined";
                var boardField = new BoardField(i, j, lastUsed, !hasTile);
                var boardFieldElement = boardField.getElement();

                if (hasTile) {
                    var tile = new Tile(boardTiles[i][j][0], boardTiles[i][j][1]);
                    boardFieldElement.appendChild(tile.getElement());
                    if (this.adjustDimensions(j, i)) {
                        adjustedDimensions = true;
                    }
                }
                divRow.appendChild(boardFieldElement);
            }
            newBoard.appendChild(divRow);
        }
        return adjustedDimensions ? this.getElement(boardTiles, lastUsedTiles) : newBoard;
    }
}

class Score {
    getElement(playerData, currentPlayer, ownPlayer, showDetail) {
        var newScoreGrid = new DocumentFragment();

        // Header
        if (showDetail) {
            var turnHeader = createElem("div", {}, ["score-header"], i18n("round"));
            newScoreGrid.appendChild(turnHeader);
        }
        for (var i = 0; i < playerData.length; i++) {
            const player = playerData[i];
            const lastScore = player.scoreValues.length ? " (+" + player.scoreValues[player.scoreValues.length - 1] + ")" : "";
            var scoreHeader = createElem("div", {}, ["score-header", player.id === currentPlayer ? "highlight" : null, player.id === ownPlayer ? "own-player" : null], player["name"] + ": " + player["score"] + lastScore);
            newScoreGrid.appendChild(scoreHeader);
        }

        if (!showDetail) {
            return newScoreGrid;
        }


        var handCountHeader = createElem("div");
        newScoreGrid.appendChild(handCountHeader);
        // hand count
        for (var i = 0; i < playerData.length; i++) {
            var handCounters = createElem("div", {}, ["text-center", "break-all"]);
            for (var j = 0; j < playerData[i].handCount; j++) {
                var handCountValue = createElem("span", {}, [(j >= playerData[i].handCount - playerData[i].lastSwapCount) ? "text-red-600" : null], "");
                handCountValue.innerHTML = "&#9632;";
                handCounters.appendChild(handCountValue);
            }
            newScoreGrid.appendChild(handCounters);
        }

        // score-cells
        for (var j = 0; j < playerData[0].scoreValues.length; j++) {
            var turnCell = createElem("div", {}, ["text-center"], (j + 1));
            newScoreGrid.appendChild(turnCell);

            for (var i = 0; i < playerData.length; i++) {
                var scoreCell = createElem("div", {}, ["text-center"], playerData[i].scoreValues[j]);
                newScoreGrid.appendChild(scoreCell);
            }
        }
        return newScoreGrid;
    }
}

class Player {
    getHandElement(playerData) {
        var newHand = new DocumentFragment();
        for (var j = 0; j < playerData.hand.length; j++) {
            let hf = new HandField(j >= playerData.hand.length - playerData.lastSwapCount);
            let el = hf.getElement();
            let tile = new Tile(playerData.hand[j][0], playerData.hand[j][1], j, playerData.isOnTurn);
            el.appendChild(tile.getElement());
            newHand.appendChild(el);
        }
        return newHand;
    }
}

class Tile {
    constructor(color, shape, id = null, draggable = false) {
        this._el = createElem("div", {"draggable": draggable, "data-value-c": color, "data-value-s": shape},
                ["tile", "shape-" + shape, "color-" + color]);
        if (id !== null) {
            this._el.setAttribute("id", "tile-" + id);
        }
        if (draggable) {
            this._el.addEventListener("dragstart", this.dragStart);
    }
    }

    getElement() {
        return this._el;
    }

    dragStart(e) {
        e.dataTransfer.effectAllowed = "move";
        e.dataTransfer.setData("text", e.target.getAttribute("id"));
    }
}

class BoardField {
    constructor(x, y, used = false, draggable = true) {
        this._el = createElem("div", {"data-field-x": x, "data-field-y": y}, ["field", used ? "used" : null]);
        if (draggable) {
            this._el.addEventListener("dragover", this.dragover);
            this._el.addEventListener("drop", this.drop);
    }
    }

    dragover(e) {
        e.preventDefault();
    }
    drop(e) {
        e.preventDefault();
        if (this.children.length) {
            return;
        }
        const id = e.dataTransfer.getData("text");
        const draggedEl = document.getElementById(id);
        draggedEl.setAttribute("data-field-x", e.target.getAttribute("data-field-x"));
        draggedEl.setAttribute("data-field-y", e.target.getAttribute("data-field-y"));
        draggedEl.classList.remove("swapped");
        draggedEl.classList.add("played");
        this.appendChild(draggedEl);
    }

    getElement() {
        return this._el;
    }
}

class HandField {
    constructor(isNew = false) {
        this._el = createElem("div", {}, ["field", isNew ? "used" : null]);
        this._el.addEventListener("dragover", this.dragover);
        this._el.addEventListener("drop", this.drop);
    }

    dragover(e) {
        e.preventDefault();
    }
    drop(e) {
        e.preventDefault();
        if (this.children.length) {
            return;
        }
        const id = e.dataTransfer.getData("text");
        const draggedEl = document.getElementById(id);
        draggedEl.classList.remove("played", "swapped");
        this.appendChild(draggedEl);
    }

    getElement() {
        return this._el;
    }
}

class SwapField {
    constructor() {
        this._el = createElem("div", {}, ["field"]);
        this._el.addEventListener("dragover", this.dragover);
        this._el.addEventListener("drop", this.drop);
    }

    dragover(e) {
        e.preventDefault();
    }
    drop(e) {
        e.preventDefault();
        const id = e.dataTransfer.getData("text");
        const draggedEl = document.getElementById(id);
        draggedEl.classList.remove("played");
        draggedEl.classList.add("swapped");
        this.appendChild(draggedEl);
    }

    getElement(textContent = "", highlight = false) {
        const contentElement = createElem("div", {}, ["w-full"], textContent);
        if (highlight) {
            this._el.classList.add("bg-yellow-200");
        } else {
            this._el.classList.remove("bg-yellow-200");
        }
        redrawElement(this._el, contentElement);
        return this._el;
    }
}