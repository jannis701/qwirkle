export const localeData = {
    "values": {
        "langId": "en-US",
        "cancel": "Cancel",
        "save": "Save",
        "swap": "swap",
        "errorTitle": "Error",
        "successTitle": "Success",
        "warning": "Warning",
        "ok": "OK",
        "confirmToProceed": "Confirm to proceed",
        "gameId": "game-ID",
        "join": "join",
        "name": "name",
        "leaveGame": "leave game",
        "startGame": "start game",
        "gameFinished": "The end of the game is reached.",
        "play": "play",
        "newGame": "new game",
        "Invalid position for tiles.": "Invalid position for tiles.",
        "round": "round",
        "players": "players",
        "inviteplayers": "Invite players",
        "qwirkleSize": "number of colors/shapes",
        "Invalid combination of tiles.": "Invalid combination of tiles."
    }
}
