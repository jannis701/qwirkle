export const localeData = {
    "values": {
        "langId": "de-DE",
        "cancel": "Abbrechen",
        "save": "Speichern",
        "swap": "tauschen",
        "errorTitle": "Fehler",
        "successTitle": "OK",
        "warning": "Achtung",
        "ok": "OK",
        "confirmToProceed": "Willst du fortfahren?",
        "gameId": "Spiel-ID",
        "join": "Beitreten",
        "name": "Name",
        "leaveGame": "Spiel verlassen",
        "startGame": "Spiel starten",
        "gameFinished": "Das Spielende ist erreicht.",
        "play": "spielen",
        "newGame": "neues Spiel",
        "Invalid position for tiles.": "Ungültiger Spielzug.",
        "round": "Runde",
        "players": "Spieler",
        "inviteplayers": "Spieler einladen",
        "qwirkleSize": "Anzahl Farben/Formen",
        "Invalid combination of tiles.": "Ungültige Kombination von Spielsteinen."
    }
}
