export function createElem(type, attributes = {}, classList = [], textContent = null) {
    var element = document.createElement(type);
    return modifyElem(element, attributes, classList, textContent);
}

export function modifyElem(element, attributes = {}, classList = [], textContent = null, parent = document) {
    element = loadElement(element, parent);
    for (let key in attributes) {
        element.setAttribute(key, attributes[key]);
    }
    for (let c in classList) {
        if (classList[c]) {
            element.classList.add(classList[c]);
        }
    }
    if (textContent !== null) {
        element.textContent = textContent;
    }
    return element;
}

export function loadElement(element, parent = document) {
    if (typeof element === "string") {
        element = parent.getElementById(element);
    }
    return element;
}

export function loadTemplate(templateName) {
    const parser = new DOMParser();
    const templatePath = "./js/template_" + templateName + ".html";
    return fetch(templatePath).then(response => response.blob())
            .then(blob => blob.text())
            .then(text => parser.parseFromString(text, "text/html"));
}

export function redrawElement(element, newContent) {
    while (element.firstChild) {
        element.firstChild.remove();
    }
    element.appendChild(newContent);
}

export function encodeTile(color, shape) {
    return color + "_" + shape;
}