<?php

require_once "../src-php/autoload.php";
require_once "../src-php/GameRouter.php";

session_start();

$DATA_PATH = "../data";

// routing starten
$router = new GameRouter($DATA_PATH);
$router->runActionJson($router->getParam("game_id") ?: "", $router->getParam("cmd") ?: "");
