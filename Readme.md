### Qwirkle
Multiplayer only in-browser-game Qwirkle.

* Rules and description: https://www.mindware.orientaltrading.com/qwirkle-with-free-bonus-pack-a2-13791375.fltr
* Rules and description in German: https://www.schmidtspiele.de/details/produkt/Qwirkle.html

Demo: https://js.uber.space/qwirkle

Share the link https://js.uber.space/qwirkle/#XXXXXXX which is generated to play
together. All players have to agree on the number of different colors/shapes.


### Deploy
Copy the folders `public_html` and `src-php` to a php-capable webserver and create 
the directory `data`, which has to be writeable to the server.

Pathes in `public_html/cmd.php` may have to be customized.

I recommend to place `data` and `src-php` to places on the server without public 
access.

### Develop

* `npm install` or `make` to install tailwindcss. 
* `make build-css` or `npm run build-css`, to generate CSS from `src-css/tailwind.css`.
* `make php-test` to run php-tests (requires phpunit-9.5.phar)
* `make serve` to run the app in a php-dev-server on http://localhost:4123

#### Files an folders
* `public_html`: javascript-frontend and backend-entry-point `cmd.php` 
* `src-css`: tailwind-source-files to create `public_html/css/app-tailwind.css`
* `src-php`: classes used by the php-backend
* `test-php`: php-unit-tests for the php-backend
* `data`: directory to save game-states. one file per game.

#### Known limitations/bugs/todos:
* even if the case is kind of considered in the backend, it is not yet implemented 
  in the frontend, that a player leaves the game and the others continue playing
* source-code is commented and no documentation yet
* synchronization of the game-state via repeated requests to the backend. no sockets.

### License and used software

The software itself is licensed unter the MIT License (see [License.txt](License.txt)).

The following software is used in this project:

* i18njs ( https://github.com/roddeh/i18njs/ ), MIT License

The following software is used in the development of this project:
* tailwindcss ( https://tailwindcss.com/ )
* phpunit 9.5 ( https://github.com/sebastianbergmann/phpunit )