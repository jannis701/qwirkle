module.exports = {
  mode: 'jit',
  content: [
      './public_html/index.html',
      './public_html/js/*.html',
      './public_html/js/app.js',
      './src-css/safelist.txt'
  ],
  theme: {
    extend: {},
  },
  variants: {
    extend: {
        backgroundColor: ['disabled']
    },
  },
  plugins: [],
}
