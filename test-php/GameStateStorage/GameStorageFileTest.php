<?php

use PHPUnit\Framework\TestCase;
use GameStateStorage\GameStorageFile;

class GameStorageFileTest extends TestCase {

    public function testFileLock(): void {
        $path = __DIR__ . "/../../data";

        $game_id = "storagetest1";

        $storage = new GameStorageFile($game_id, ["path" => $path]);

        $this->expectException(ErrorException::class);
        $storage->writeGameData("bla");
    }

    public function testGameStorage(): void {
        $path = __DIR__ . "/../../data";

        $game_id = "storagetest2";
        $test_content = "blubb";

        $storage = new GameStorageFile($game_id, ["path" => $path]);
        $storage->lockGameData();

        $storage->writeGameData($test_content);
        $storage->unlockGameData();

        $storage2 = new GameStorageFile($game_id, ["path" => $path]);
        $content2 = $storage2->readGameData();

        $this->assertEquals(
                $test_content,
                $content2
        );
    }
}
