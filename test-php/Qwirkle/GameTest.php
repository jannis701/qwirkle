<?php

use PHPUnit\Framework\TestCase;
use Qwirkle\Game;
use Qwirkle\Player;
use Qwirkle\Hand;

class GameTest extends TestCase {

    public function testPlayerCanBeAdded(): void {

        $game = new Game("test.GAME2");
        $p1 = new Player("Egon Pan");
        $game->addPlayer($p1);

        // print $game;
        $p2 = new Player("Peter Pan");
        $game->addPlayer($p2);
        // print $game;

        $this->expectException(InvalidArgumentException::class);
        $p3 = new Player("Egon Pan");
        $game->addPlayer($p3);
    }

    public function testValidMoveIsPossible(): void {

        $game = new Game("test.GAME2");
        $p1 = new Player("Egon Pan");
        $p1_id = $game->addPlayer($p1);
        $p2 = new Player("Peter Pan");
        $p2_id = $game->addPlayer($p2);

        $game->startGame();

        $test_hand1 = new Hand([[1, 1], [1, 2], [1, 3], [2, 1], [3, 1], [3, 2]]);
        $test_hand2 = new Hand([[3, 3], [3, 5], [1, 3], [2, 1], [3, 1], [3, 2]]);

        // it is already decided, which player starts the game by their original 
        // hands. make p1 the starting player:
        if ($game->getStatus($p1_id)["playerOnTurn"] !== $p1_id) {
            list($p1_id, $p2_id) = [$p2_id, $p1_id];
            list($p1, $p2) = [$p2, $p1];
        }

        $p1->setHandDebug($test_hand1);
        $tbc = [];
        $tbc[5][5] = [1, 1];
        $tbc[5][6] = [1, 2];
        $tbc[5][7] = [1, 3];
        $game->playTiles($tbc, $p1_id);

        $p2->setHandDebug($test_hand2);
        $tbc2 = [];
        $tbc2[4][5] = [2, 1];
        $tbc2[6][5] = [3, 1];
        $game->playTiles($tbc2, $p2_id);

        $tbc3 = [];
        $tbc3[6][6] = [3, 2];
        $game->playTiles($tbc3, $p1_id);

        $tbc4 = [];
        $tbc4[6][7] = [3, 3];
        $tbc4[6][8] = [3, 5];
        $game->playTiles($tbc4, $p2_id);

        // print $game;

        $this->expectException(ErrorException::class);
        $p1->setHandDebug($test_hand1);

        $tbc4 = [];
        $tbc4[4][7] = [1, 1];
        $tbc4[6][7] = [1, 2];
        $tbc4[8][7] = [1, 3];
        $game->playTiles($tbc4, $p1_id);
    }

}
