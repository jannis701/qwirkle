<?php

use PHPUnit\Framework\TestCase;
use Qwirkle\Board;
use Qwirkle\Point2D;

class BoardTest extends TestCase {

    public $qwirkle_size = 6;

    public function testNoInvalidPutPossible() {
        $board = new Board($this->qwirkle_size);

        $board->putTile(0, 0, 1, 1);
        // print "\n" . $board;

        $this->expectException(InvalidArgumentException::class);
        $board->putTile(0, 1, 1, $this->qwirkle_size);
        $this->expectException(InvalidArgumentException::class);
        $board->putTile(0, 1, $this->qwirkle_size, 1);
    }

}
