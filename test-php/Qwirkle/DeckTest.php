<?php

use PHPUnit\Framework\TestCase;
use Qwirkle\Deck;

class DeckTest extends TestCase {

    private $qwirkle_size = 6;

    public function testTilesCanBeDrawn(): void {
        $deck = new Deck($this->qwirkle_size);
        $deck->drawTiles(4);
        $this->assertEquals(
                $this->qwirkle_size * $this->qwirkle_size * 3 - 4,
                $deck->remaining()
        );

        $tiles = $deck->drawTiles($this->qwirkle_size * $this->qwirkle_size * 3);
        $this->assertEquals(
                $this->qwirkle_size * $this->qwirkle_size * 3 - 4,
                count($tiles)
        );
        $this->assertEquals(
                0,
                $deck->remaining()
        );
    }

    public function testTilesCanBeAdded(): void {
        $deck = new Deck($this->qwirkle_size);
        $tiles = $deck->drawTiles(7);
        $deck->addTiles([$tiles[0], $tiles[2]]);
        $this->assertEquals(
                $this->qwirkle_size * $this->qwirkle_size * 3 - 7 + 2,
                $deck->remaining()
        );
    }

}
