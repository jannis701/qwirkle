install:
	npm install

build-css:
	npm run build-css

php-test:
	php phpunit-9.5.phar test-php/ --bootstrap=src-php/autoload.php

serve:
	php -S localhost:4123 -t public_html
